"""
This module loads the complete Zotero bibliography of the EUPT
project and renders it in HTML.
"""

import re
from datetime import datetime
from pyzotero import zotero
from dominate.tags import span, div, ul, li, sup


def load_data() -> list:
    """
    Loads the contents of the Zotero library.
    """
    library_id = '5113405'
    library_type = 'group'
    zot = zotero.Zotero(library_id, library_type, '')
    items = zot.everything(zot.items())
    return items


def get_creator(entry: dict, creator_type: str, order: str = 'sur_first') -> span | None:
    """
    Retrieves the creator(s) for a given item. Creators can be
    either authors or editors, depending on the context.

    Args:
        - entry: the bibliography entry
        - creator_type: 'author' or 'editor'
        - order: indicates in which order the name should be

    Return
        - with order='fore_first': dominate.span with the creator string (e.g. 'A. Foo / B. Bar')
        - with order='sur_first': dominate.span with the creator string (e.g. 'Foo, A. / Bar, B.')
    """

    creators = entry['data']['creators']
    auths = []
    for creator in creators:
        if creator['creatorType'] == creator_type:
            first_name = make_short_first_name(creator["firstName"])
            last_name = creator["lastName"]
            if order == 'sur_first':
                name = f'{last_name}, {first_name}'
                css_class = creator_type
            else:
                # only applies when the entry is a publication in an anthology.
                # in this case the anthology's publishers are names, but with forenames
                # first. also, they should not be displayed in italics. therefore we need
                # a separate CSS class.
                name = f'{first_name} {last_name}'
                css_class = 'editors'
            auths.append(name)
    if len(auths) > 0:
        return span(" / ".join(auths), cls=f'{css_class}')
    return None


def make_short_first_name(first_name: str) -> str:
    """
    Creates an abbreviated version of one or more first names.
    
    Examples:
        - 'Aurelius' --> 'A.'
        - 'Marcus Tullius' --> 'M. T.'
    """
    split = first_name.split(' ')
    first_chars = [f'{name[0]}.' for name in split]
    return ' '.join(first_chars)


def get_title(entry: dict, zot_title_type: str, css_class: str) -> span:
    """
    Retrieves the title for a given item.
    The 'title' field is used ambiguosly in Zotero: it refers to a journal article's
    title as well as a book title when the whole book is referenced, but also to a book
    section's title when only a book section is referenced (in this case the book title
    can be found in the 'bookTitle' field).
    To simplify our CSS styles, we pass a css_class argument which sets the CSS class.

    Args:
        - entry: the bibliography entry
        - title_type: 'title' or 'bookTitle'
        - css_class: the name of the CSS class created
    Return
        - dominate.span with the title string
    """
    title = entry['data'][zot_title_type]

    # convert HTML tags in title to dominate objects
    hi_pattern = re.compile('(.*?<i>.*?</i>.*?)+')
    sup_pattern = re.compile('(.*?<sup>.*?</sup>.*?)+')
    if hi_pattern.match(title) or sup_pattern.match(title):
        subst_hi = re.sub(r'</?i>', '#', title)
        subst_sup = re.sub(r'</?sup>', '@', subst_hi)
        title = subst_sup.split('#')
        for i, title_part in enumerate(title):
            if '@' in title_part:
                sup_parts = title_part.split('@')
                for j, sup_part in enumerate(sup_parts):
                    if j % 2 == 1:
                        sup_parts[j] = sup(sup_part)
                if i % 2 == 1:
                    title[i] = span(sup_parts, cls='hi')
                else:
                    title[i] = sup_parts
            else:
                if i % 2 == 1:
                    title[i] = span(title_part, cls='hi')

    # strip last dot to avoid duplication
    if title and isinstance(title[-1], str) and title[-1].endswith('.'):
        return span(title[:-1], cls=css_class)
    return span(title, cls=css_class)


def get_attribute(entry: dict, attr_name: str) -> span | None:
    """
    Retrieves an attribute of the given entry.
    Args:
        - entry: the bibliography entry
        - attr_name: the attribute's name, e.g. 'place'
    Return
        - dominate.span with the attribute value as string
    """
    try:
        if not entry['data'][attr_name]:
            return ''
        return span(entry['data'][attr_name], cls=f'{attr_name}')
    except KeyError:
        return None


def make_book_ref(entry: dict) -> set | None:
    """
    Creates a reference to a monography. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    We currently consider the following cases (no mark up for easy readibility):

    W/o editors:
        - Foo, A., 2024: My title. Göttingen. (default return)
        - Foo, A., 2024: My title. Some Series 1. Göttingen: Univerisätsverlag. 
            (series and publisher)
        - Foo, A., 2024: My title. Göttingen: Universitätsverlag.
            (publisher only)
        - Foo, A., 2024: My title. Some Series 1. Göttingen. (series only)
    With editors:
        - Bar, B., ed. 2024: My Collection. Göttingen. (default)
        - Bar, B., ed. 2024: My Collection. Some Series 1. Göttingen: Universitätsverlag.
            (series and publisher)
        - Bar, B., ed. 2024: My Collection. Some Series 1. Göttingen. (series)
        - Bar, B., ed. 2024: My Collection. Göttingen: Universitätsverlag. (publisher)

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """

    authors = get_creator(entry, 'author')
    editors = get_creator(entry, 'editor')
    title = get_title(entry, 'title', 'book_title')
    place = get_attribute(entry, 'place')
    pub = get_attribute(entry, 'publisher')
    year = get_attribute(entry, 'date')
    series = get_attribute(entry, 'series')
    series_no = get_attribute(entry, 'seriesNumber')

    if editors:
        resp = editors
        year_seperator = ', ed. '
    else:
        resp = authors
        year_seperator = ', '

    fixed_part = (resp, year_seperator, year, ': ', title, '. ')
    if pub and series and series_no:
        return (fixed_part, series, ' ', series_no, '. ', place, ': ', pub, '.')
    if pub and series:
        return (fixed_part, series, ' ', span(cls='empty'), '. ', place, ': ', pub, '.')
    if series and series_no:
        return (fixed_part, series, ' ', series_no, '. ', place, '.')
    if series:
        return (fixed_part, series, ' ', span(cls='empty'), '. ', place, '.')
    if pub:
        return (fixed_part, '. ', place, ': ', pub, '.')
    return (fixed_part, place, '.')


def make_journal_ref(entry: dict) -> set:
    """
    Creates a reference to a journal article. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    Journal articles have the following format (no mark up for easy readibility):

    Foo, A., 2024: "Title of my Article". Journal 4: 1–25.

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """

    authors = get_creator(entry, 'author')
    title = get_title(entry, 'title', 'title')
    date = get_attribute(entry, 'date')
    journal = span(entry['data']['publicationTitle'], cls='journal')
    vol = span(entry['data']['volume'], cls='vol')
    pp = get_attribute(entry, 'pages')

    if pp:
        return (authors, ', ', date, ': "', title, '." ', journal, ' ', vol, ': ', pp, '.')
    return (authors, ', ', date, ': "', title, '." ', journal, ' ', vol, ': ', span(cls='empty'), '.')


def make_book_section_ref(entry: dict) -> set | None:
    """
    Creates a reference to a book section. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    We currently consider the following cases (no mark up for easy readibility):

    With authors:
        - Foo, A. / Bar B., Im Druck: "My Paper Title". Some Book Title, edited by C. Baz. Göttingen.
            (default)
        - Foo, A. / Bar B., 2024: "My Paper Title". Some Book Title, edited by C. Baz. 20–35. 
            Some Series 1. Göttingen: Universitätsverlag. (series and publisher)
        - Foo, A. / Bar B., 2024: "My Paper Title". Some Book Title, edited by C. Baz. 20–35. 
            Göttingen: Universitätsverlag. (publisher)
        - Foo, A. / Bar B., 2024: "My Paper Title". Some Book Title, edited by C. Baz. 20–35. 
            Some Series 1. Göttingen. (series)

    With editors only:
        - Foo, A. / Bar. B., 2024: My Book Title, 50–77. Göttingen. (default)
        - Foo, A. / Bar. B., 2024: My Book Title, 50–77. Some Series 7. Göttingen: 
            Universitätsverlag. (series and publisher)
        - Foo, A. / Bar. B., 2024: My Book Title, 50–77. Göttingen: Universitätsverlag.
            (publisher)
        - Foo, A. / Bar. B., 2024: My Book Title, 50–77. Some Series 7. Göttingen. (series)

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """

    authors = get_creator(entry, 'author')
    editors = get_creator(entry, 'editor')
    title = get_title(entry, 'title', 'title')
    if title:
        place = get_attribute(entry, 'place')
        pub = get_attribute(entry, 'publisher')
        date = get_attribute(entry, 'date')
        series = get_attribute(entry, 'series')
        series_no = get_attribute(entry, 'seriesNumber')
        book_title = get_title(entry, 'bookTitle', 'book_title')
        pp = get_attribute(entry, 'pages')

        if authors and editors:
            inner_editors = get_creator(entry, 'editor', 'fore_first')
            # in case of the works still being in printing we don't have pages yet.
            if pp:
                fixed_part = (authors, ', ', date, ': "', title, '." ',
                              book_title, ', edited by ', inner_editors, ', ', pp, '.')
            else:
                fixed_part = (authors, ', ', date, ': "', title, '." ',
                              book_title, ', edited by ', inner_editors, '.')

            if pub and series and series_no:
                return (fixed_part, ' ', series, ' ', series_no, '. ', place, ': ', pub, '.')
            if pub and series:
                return (fixed_part, ' ', series, ' ', span(cls='empty'), '. ', place, ': ', pub, '.')
            if pub:
                return (fixed_part, ' ', place, ': ', pub, '.')
            if series and series_no:
                return (fixed_part, ' ', series, ' ', series_no, '. ', place, '.')
            if series:
                return (fixed_part, ' ', series, ' ', span(cls='empty'), '. ', place, '.')
            return (fixed_part, ' ', place, '.')

        if authors:
            if pp:
                pages = pp
            else:
                pages = span(cls='empty')
            fixed_part = (authors, ', ', date, ': ', book_title, ', ', pages, '. ')
            if pub and series:
                return (fixed_part, series, '. ', place, ': ', pub, '.')
            if pub:
                return (fixed_part, place, ': ', pub, '.')
            if series:
                return (fixed_part, series, '. ', place, '.')

            return (fixed_part, place, '.')
    return None


def make_encycl_ref(entry: dict) -> set | None:
    """
    Creates a reference to an encyclopedia article. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    Examples:
    - Nachname, Vorname, Jahr: "Titel des Artikels." In Titel des Lexikons Bandummer, edited by Vorname Nachname / Vorname Nachname [wenn editors angegeben sind], Seitenzahl.
    - van Soldt, W. H., 2014: "Ugarit. A. Geschichte und Literatur." In RlA 14, 280–283.
    - Grimm, G. E., 2007: "Akrostichon." In Metzler Lexikon Literatur. Begriffe und Definitionen. 3., völlig neu bearbeitete Auflage, edited by D. Burdorf / C. Fasbender / B. Moennighoff, 9.

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """
    authors = get_creator(entry, 'author')
    inner_editors = get_creator(entry, 'editor', 'fore_first')
    title = get_title(entry, 'title', 'title')
    book_title = get_title(entry, 'encyclopediaTitle', 'book_title')
    pp = get_attribute(entry, 'pages')
    date = get_attribute(entry, 'date')
    edition = get_attribute(entry, 'edition')
    volume = get_attribute(entry, 'volume')

    first_fixed_part = (authors, ', ', date, ': "', title, '." In ', book_title)
    if inner_editors and edition:
        fixed_part = (first_fixed_part, '. ', edition, ', edited by ', inner_editors)
    elif volume:
        fixed_part = (first_fixed_part, ' ', volume)
    else:
        fixed_part = first_fixed_part
    if pp:
        return (fixed_part, ', ', pp, '.')
    return (fixed_part, '.')


def make_thesis_ref(entry: dict) -> set:
    """
    Creates a reference to a thesis. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    Examples:
    - Nachname, Vorname, Jahr: Titel der Thesis. Type, University.
    - Burlingame, A, 2021: Ugaritic Indefinite Pronouns: Linguistic, Social, and 
        Textual Perspectives. Unpubl. dissertation, University of Chicago.

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """
    authors = get_creator(entry, 'author')
    book_title = get_title(entry, 'title', 'book_title')
    date = get_attribute(entry, 'date')
    thesis_type = get_attribute(entry, 'thesisType')
    university = get_attribute(entry, 'university')

    return (authors, ', ', date, ': ', book_title, '. ', thesis_type, ', ', university, '.')


def make_manuscript_ref(entry: dict) -> set:
    """
    Creates a reference to a manuscript. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    Examples:
    - Nachname, Vorname, Jahr: Titel der Thesis. Type.
    - Sarlo, D., 2013: What do the Dead Know? Ancestor Worship and Necromancy in Ancient Israel. Seminar paper.

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """
    authors = get_creator(entry, 'author')
    book_title = get_title(entry, 'title', 'book_title')
    date = get_attribute(entry, 'date')
    type = get_attribute(entry, 'manuscriptType')

    return (authors, ', ', date, ': ', book_title, '. ', type, '.')


def make_webpage_ref(entry: dict) -> set:
    """
    Creates a reference to a webpage. Each part of the bibliographic entry
    (e.g. the publication date) is wrapped in a dominate.span with a fitting class
    so that CSS rules can be easily applied.

    Examples:
    - Autor*in, Jahr: Titel der webpage (vollständige URL). Datum des letzten Zugriffs: dateModified.
    - Foo, B., 2024: Some Website (https://text.com). Datum des letzten Zugriffs: 21.05.2024.

    Args:
        - entry: the current bibliographic entry
    Return:
        - a set with the marked up bibliographic reference (see above)
    """
    authors = get_creator(entry, 'author')
    book_title = get_title(entry, 'title', 'book_title')
    date = get_attribute(entry, 'date')
    url = get_attribute(entry, 'url')
    modified = get_attribute(entry, 'dateModified')
    mod_date = entry['data']['dateModified'].split('T')[0]
    mod_date_datetime = datetime.strptime(mod_date, '%Y-%m-%d')
    s_modified = datetime.strftime(mod_date_datetime, '%d.%m.%Y')
    modified = span(s_modified, cls='dateModified')

    return (authors, ', ', date, ': ', book_title, ' (', url, '). Datum des letzten Zugriffs: ', modified, '.')


def make_ref_by_id(zotero_id: str, data: list) -> str:
    """
    Makes a bibliographical reference for an item with a given ID.

    Args:
        - zotero_id: The ID 
        - data: the complete Zotero bibliography as list of dicts
    """
    entry = next((item for item in data if item['key'] == zotero_id), None)
    if entry:
        pub_type = entry['data']['itemType']

        if pub_type == 'book':
            cit = make_book_ref(entry)

        elif pub_type == 'journalArticle':
            cit = make_journal_ref(entry)

        elif pub_type == 'bookSection':
            cit = make_book_section_ref(entry)

        elif pub_type == 'encyclopediaArticle':
            cit = make_encycl_ref(entry)

        elif pub_type == 'thesis':
            cit = make_thesis_ref(entry)

        elif pub_type == 'manuscript':
            cit = make_manuscript_ref(entry)

        elif pub_type == 'webpage':
            cit = make_webpage_ref(entry)
        else:
            print(
                f'Unknown publication type found in {zotero_id}: {pub_type}')
        try:
            return div(cit, data_zotero=f'eupt:{zotero_id}', __pretty=False)
        except UnboundLocalError:
            print(f'Unknown publication type found in {zotero_id}: {pub_type}')

    print(f'No entry created for {zotero_id}')


if __name__ == "__main__":
    entries = []
    data = load_data()
    for item in data:
        key = item['key']
        try:
            entries.append(make_ref_by_id(key, data))
        except KeyError:
            print(f'No creator found for the item with key {key}')
    with open('bibliography.html', mode='w', encoding='utf-8') as f:
        html_data = ul(li(e) for e in entries)
        f.write(str(html_data))
