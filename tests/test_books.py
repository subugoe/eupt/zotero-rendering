import json
import re

from src.biblio import make_ref_by_id


with open('tests/items.json', mode='r', encoding='utf-8') as f:
    DATA = json.load(f)


def normalize(text: str) -> str:
    """
    Removes all unnecessary white spaces from a string.
    """
    pattern = re.compile(r'\s+')
    normalized = re.sub(pattern, ' ', text).strip(
    ).translate({ord(c): None for c in '\n\t\r'})
    return normalized


def test_books_wo_series():
    result = make_ref_by_id('5L7QK2QK', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:5L7QK2QK"><span class="author">Ajjan, L.</span>, <span class="date">1983</span>: <span class="book_title">Notes ugaritiques</span>. <span class="place">Doha</span>.</div>'


def test_books_with_series():
    result = make_ref_by_id('ZHL7WM8M', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:ZHL7WM8M"><span class="author">Gröndahl, F.</span>, <span class="date">1967</span>: <span class="book_title">Die Personennamen der Texte aus Ugarit</span>. <span class="series">StPohl</span> <span class="seriesNumber">1</span>. <span class="place">Rom</span>.</div>'


def test_books_with_publisher():
    result = make_ref_by_id('KHASI7LJ', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:KHASI7LJ"><span class="author">Cohen, C. / Sivan, D.</span>, <span class="date">1983</span>: <span class="book_title">The Ugaritic Hippiatric Texts. A Critical Edition</span>. <span class="series">AOS Essay</span> <span class="seriesNumber">9</span>. <span class="place">New Haven, Conn</span>: <span class="publisher">American Oriental Society</span>.</div>'


def test_book_section_with_series_with_seriesNumber():
    result = make_ref_by_id('29QH3V3P', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:29QH3V3P"><span class="author">Spronk, K.</span>, <span class="date">1999</span>: &quot;<span class="title">The Incantations</span>.&quot; <span class="book_title">Handbook of Ugaritic Studies</span>, edited by <span class="editors">W. G. E. Watson / N. Wyatt</span>, <span class="pages">270–286</span>. <span class="series">HdO</span> <span class="seriesNumber">I 39</span>. <span class="place">Leiden / Boston / Köln</span>.</div>'


def test_book_section_wo_series():
    result = make_ref_by_id('SBGG6DIB', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:SBGG6DIB"><span class="author">Dietrich, M. / Loretz, O.</span>, <span class="date">1997</span>: &quot;<span class="title">Mythen und Epen in ugaritischer Sprache</span>.&quot; <span class="book_title">Texte aus der Umwelt des Alten Testaments III/6. Weisheitstexte, Mythen und Epen</span>, edited by <span class="editors">O. Kaiser</span>, <span class="pages">1089-1317</span>. <span class="place">Gütersloh</span>.</div>'


def test_journal():
    result = make_ref_by_id('Z3CX32PH', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:Z3CX32PH"><span class="author">Kogan, L.</span>, <span class="date">2010</span>: &quot;<span class="title">Genealogical Position of Ugaritic: the Lexical Dimension. Lexical Isoglosses between Ugaritic and other Semitic Languages</span>.&quot; <span class="journal">Sefarad</span> <span class="vol">70</span>: <span class="pages">279-308</span>.</div>'


def test_trailing_dot():
    result = make_ref_by_id('9XDW9CBZ', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:9XDW9CBZ"><span class="author">Schuster-Brandis, A.</span>, <span class="date">2008</span>: <span class="book_title">Steine als Schutz- und Heilmittel. Untersuchungen zu ihrer Verwendung in der Beschwörungskunst Mesopotamiens im 1. Jt. v. Chr</span>. <span class="series">AOAT</span> <span class="seriesNumber">46</span>. <span class="place">Münster</span>.</div>'


def test_in_printing():
    result = make_ref_by_id('EEZIJC56', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:EEZIJC56"><span class="author">Vayntrub, J.</span>, <span class="date">2022</span>: &quot;<span class="title">Transmission and Mortal Anxiety in the Tale of Aqhat</span>.&quot; <span class="book_title">“Like ˀIlu Are You Wise”. Studies in Northwest Semitic Languages and Literatures in Honor of Dennis G. Pardee</span>, edited by <span class="editors">H. H. Hardy, II / J. Lam / E. D. Reymond</span>, <span class="pages">73–87</span>. <span class="place">Chicago</span>.</div>'


def test_editor():
    result = make_ref_by_id('5S9ZUQAZ', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:5S9ZUQAZ"><span class="editor">Young, G. D.</span>, ed. <span class="date">1981</span>: <span class="book_title">Ugarit in Retrospect. Fifty Years of Ugarit and Ugaritic</span>. <span class="place">Winona Lake, IN</span>.</div>'


def test_highlighted_parts_in_title():
    result = make_ref_by_id('NBXMS58Q', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:NBXMS58Q"><span class="author">Neu, E.</span>, <span class="date">1995</span>: &quot;<span class="title">Hethitisch <span class="hi">zapzagi-</span></span>.&quot; <span class="journal">Ugarit-Forschungen</span> <span class="vol">27</span>: <span class="pages">395\u2013402</span>.</div>'


def test_highlighted_parts_in_book_title():
    result = make_ref_by_id('M8NINXGG', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:M8NINXGG"><span class="author">Margalit, B.</span>, <span class="date">1980</span>: &quot;<span class="title">Death and Dying in the Ugaritic Epics</span>.&quot; <span class="book_title">Death in Mesopotamia. Papers read at the XXVI<span class="hi"><sup>e</sup> Rencontre Assyriologique internationale</span></span>, edited by <span class="editors">B. Alster</span>, <span class="pages">243–254</span>. <span class="series">Mesopotamia</span> <span class="seriesNumber">8</span>. <span class="place">Kopenhagen</span>.</div>'


def test_unknown():
    result = make_ref_by_id('KBDTQ8BA', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:KBDTQ8BA"><span class="author">Watson, W. G. E.</span>, <span class="date">1996</span>: &quot;<span class="title">Verse Patterns in KTU 1.119:26–36</span>.&quot; <span class="journal">Studi Epigrafici e Linguistici sul Vicino Oriente Antico</span> <span class="vol">13</span>: <span class="pages">25–30</span>.</div>'


def test_sup():
    result = make_ref_by_id('C7GZNRSA', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:C7GZNRSA"><span class="author">Al-Maqdissi, M. / Calvet, Y. / Matoïan, V. / Al-Bahloul, K. / Benech, C. / Bessac, J. / Coqueugniot, É. / Geyer, B. / Goiran, J. / Marriner, N. / Onnis, F. / Sauvage, Caroline, C.</span>, <span class="date">2010</span>: &quot;<span class="title">Rapport préliminaire sur les activités de la mission syro-française de Ras Shamra-Ougarit en 2007 et 2008 (67<sup>e</sup> et 68<sup>e</sup> campagnes)</span>.&quot; <span class="journal">Syria</span> <span class="vol">87</span>: <span class="pages">21-51</span>.</div>'


def test_encyc_wo_bandnummer():
    result = make_ref_by_id('CDFUWIJB', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:CDFUWIJB"><span class="author">Grimm, G. E.</span>, <span class="date">2007</span>: &quot;<span class="title">Akrostichon</span>.&quot; In <span class="book_title">Metzler Lexikon Literatur. Begriffe und Definitionen</span>. <span class="edition">3., völlig neu bearbeitete Auflage</span>, edited by <span class="editors">D. Burdorf / C. Fasbender / B. Moennighoff</span>, <span class="pages">9</span>.</div>'


def test_encyc_wo_editors():
    result = make_ref_by_id('BT8IZDZ5', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:BT8IZDZ5"><span class="author">van Soldt, W. H.</span>, <span class="date">2014</span>: &quot;<span class="title">Ugarit. A. Geschichte und Literatur</span>.&quot; In <span class="book_title">RlA</span> <span class="volume">14</span>, <span class="pages">280–283</span>.</div>'


def test_thesis():
    result = make_ref_by_id('CBM7VZFP', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:CBM7VZFP"><span class="author">Burlingame, A.</span>, <span class="date">2021</span>: <span class="book_title">Ugaritic Indefinite Pronouns: Linguistic, Social, and Textual Perspectives</span>. <span class="thesisType">Unpubl. dissertation</span>, <span class="university">University of Chicago</span>.</div>'


def test_webpage():
    result = make_ref_by_id('4UBHHDIB', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:4UBHHDIB"><span class="author">Smith, M. S.</span>, <span class="date">2004</span>: <span class="book_title">A Bibliography of Ugaritic Grammar and Biblical Hebrew Grammar in the Twentieth Century</span> (<span class="url">http://oi-archive.uchicago.edu/OI/DEPT/RA/bibs/BH-Ugaritic.html</span>). Datum des letzten Zugriffs: <span class="dateModified">30.04.2024</span>.</div>'


def test_manuscript():
    result = make_ref_by_id('STNTXYRX', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:STNTXYRX"><span class="author">Sarlo, D.</span>, <span class="date">2013</span>: <span class="book_title">What do the Dead Know? Ancestor Worship and Necromancy in Ancient Israel</span>. <span class="manuscriptType">Seminar paper</span>.</div>'


def test_zotero_id_as_attribute():
    result = make_ref_by_id('STNTXYRX', DATA)
    assert normalize(str(result)) == '<div data-zotero="eupt:STNTXYRX"><span class="author">Sarlo, D.</span>, <span class="date">2013</span>: <span class="book_title">What do the Dead Know? Ancestor Worship and Necromancy in Ancient Israel</span>. <span class="manuscriptType">Seminar paper</span>.</div>'
